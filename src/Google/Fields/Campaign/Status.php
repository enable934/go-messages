<?php

declare(strict_types=1);

namespace Google\Fields\Campaign;

final class Status
{
    public const VALUES = [
        'UNKNOWN' => 'UNKNOWN',
        'ENABLED' => 'ENABLED',
        'PAUSED' => 'PAUSED',
        'REMOVED' => 'REMOVED',
    ];
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
