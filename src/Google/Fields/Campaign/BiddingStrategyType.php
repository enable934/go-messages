<?php

declare(strict_types=1);

namespace Google\Fields\Campaign;

final class BiddingStrategyType
{
    public const TYPES = [
        'MANUAL_CPC' => 'MANUAL_CPC',
        'MANUAL_CPM' => 'MANUAL_CPM',
        'PAGE_ONE_PROMOTED' => 'PAGE_ONE_PROMOTED',
        'TARGET_SPEND' => 'TARGET_SPEND',
        'TARGET_CPA' => 'TARGET_CPA',
        'TARGET_ROAS' => 'TARGET_ROAS',
        'MAXIMIZE_CONVERSIONS' => 'MAXIMIZE_CONVERSIONS',
        'MAXIMIZE_CONVERSION_VALUE' => 'MAXIMIZE_CONVERSION_VALUE',
        'TARGET_OUTRANK_SHARE' => 'TARGET_OUTRANK_SHARE',
        'NONE' => 'NONE',
        'UNKNOWN' => 'UNKNOWN',
    ];
    private string $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
