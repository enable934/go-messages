<?php

declare(strict_types=1);

namespace Google\Fields\Campaign;

final class AdvertisingChannelType
{
    public const TYPES = [
        'UNKNOWN' => 'UNKNOWN',
        'SEARCH' => 'SEARCH',
        'DISPLAY' => 'DISPLAY',
        'SHOPPING' => 'SHOPPING',
        'MULTI_CHANNEL' => 'MULTI_CHANNEL',
    ];
    private string $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
