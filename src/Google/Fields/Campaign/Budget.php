<?php

declare(strict_types=1);

namespace Google\Fields\Campaign;

final class Budget
{
    private string $budgetName;
    private int $amount;

    public function __construct(string $budgetName, int $amount)
    {
        $this->budgetName = $budgetName;
        $this->amount = (int) $amount * 10 ** 6;
    }

    public function getBudgetName(): string
    {
        return $this->budgetName;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
