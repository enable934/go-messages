<?php

declare(strict_types=1);

namespace Google\Fields\Campaign;

final class NetworkSetting
{
    private bool $isGoogleSearch;
    private bool $isSearchNetwork;
    private bool $isContentNetwork;

    public function __construct(bool $isGoogleSearch, bool $isSearchNetwork, bool $isContentNetwork)
    {
        $this->isGoogleSearch = $isGoogleSearch;
        $this->isSearchNetwork = $isSearchNetwork;
        $this->isContentNetwork = $isContentNetwork;
    }

    public function isGoogleSearch(): bool
    {
        return $this->isGoogleSearch;
    }

    public function isSearchNetwork(): bool
    {
        return $this->isSearchNetwork;
    }

    public function isContentNetwork(): bool
    {
        return $this->isContentNetwork;
    }
}
