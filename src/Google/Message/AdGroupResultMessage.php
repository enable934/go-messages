<?php

declare(strict_types=1);

namespace Google\Message;

final class AdGroupResultMessage extends BaseResultMessage
{
    private int $id;
    private string $name;

    public function __construct(int $id, string $name, string $guid)
    {
        parent::__construct($guid);
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
