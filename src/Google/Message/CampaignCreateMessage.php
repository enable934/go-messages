<?php

declare(strict_types=1);

namespace Google\Message;

use Google\Fields\Campaign\AdvertisingChannelType;
use Google\Fields\Campaign\BiddingStrategyType;
use Google\Fields\Campaign\Budget;
use Google\Fields\Campaign\NetworkSetting;
use Google\Fields\Campaign\Status;

final class CampaignCreateMessage extends BaseCreateMessage
{
    private string $clientCustomerId;
    private Budget $budget;
    private AdvertisingChannelType $advertisingChannelType;
    private BiddingStrategyType $biddingStrategyType;
    private ?NetworkSetting $networkSetting;
    private ?Status $status;
    private ?\DateTimeImmutable $startDate;
    private ?\DateTimeImmutable $endDate;
    private string $name;

    public function __construct(
        string $clientCustomerId,
        string $name,
        Budget $budget,
        AdvertisingChannelType $advertisingChannelType,
        BiddingStrategyType $biddingStrategyType,
        string $guid,
        NetworkSetting $networkSetting = null,
        Status $status = null,
        \DateTimeImmutable $startDate = null,
        \DateTimeImmutable $endDate = null
    ) {
        parent::__construct($guid);
        $this->clientCustomerId = $clientCustomerId;
        $this->budget = $budget;
        $this->advertisingChannelType = $advertisingChannelType;
        $this->biddingStrategyType = $biddingStrategyType;
        $this->networkSetting = $networkSetting;
        $this->status = $status;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->name = $name;
    }

    public function getClientCustomerId(): string
    {
        return $this->clientCustomerId;
    }

    public function getBudget(): Budget
    {
        return $this->budget;
    }

    public function getAdvertisingChannelType(): AdvertisingChannelType
    {
        return $this->advertisingChannelType;
    }

    public function getBiddingStrategyType(): BiddingStrategyType
    {
        return $this->biddingStrategyType;
    }

    public function getNetworkSetting(): ?NetworkSetting
    {
        return $this->networkSetting;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
