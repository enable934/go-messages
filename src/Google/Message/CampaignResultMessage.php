<?php

declare(strict_types=1);

namespace Google\Message;

final class CampaignResultMessage extends BaseResultMessage
{
    private string $name;
    private int $id;

    public function __construct(string $name, int $id, string $guid)
    {
        parent::__construct($guid);
        $this->name = $name;
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
