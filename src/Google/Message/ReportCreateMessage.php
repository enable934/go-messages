<?php

declare(strict_types=1);

namespace Google\Message;

final class ReportCreateMessage extends BaseCreateMessage
{
    private string $clientCustomerId;
    private \DateTimeImmutable $startDate;
    private \DateTimeImmutable $endDate;
    private string $name;
    private int $adId;

    public function __construct(
        string $clientCustomerId,
        \DateTimeImmutable $startDate,
        \DateTimeImmutable $endDate,
        string $name,
        int $adId,
        string $guid
    ) {
        parent::__construct($guid);
        $this->clientCustomerId = $clientCustomerId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->name = $name;
        $this->adId = $adId;
    }

    public function getClientCustomerId(): string
    {
        return $this->clientCustomerId;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAdId(): int
    {
        return $this->adId;
    }
}
