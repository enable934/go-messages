<?php

declare(strict_types=1);

namespace Google\Message;

final class CampaignGetMessage extends BaseCreateMessage
{
    private string $clientCustomerId;
    private int $campaignId;

    public function __construct(string $clientCustomerId, int $campaignId, string $guid)
    {
        parent::__construct($guid);
        $this->clientCustomerId = $clientCustomerId;
        $this->campaignId = $campaignId;
    }

    public function getClientCustomerId(): string
    {
        return $this->clientCustomerId;
    }

    public function getCampaignId(): int
    {
        return $this->campaignId;
    }
}
