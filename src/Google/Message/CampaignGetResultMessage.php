<?php

declare(strict_types=1);

namespace Google\Message;

final class CampaignGetResultMessage extends BaseResultMessage
{
    public const STATES = [
        'SERVING' => 'Кампания в настоящее время показывает рекламу.',
        'NONE' => 'Это универсальное решение, если ни один из других статусов не имеет смысла. Такая кампания не обслуживается, но ни один из других статусов не является разумным вариантом.',
        'ENDED' => 'Дата окончания кампании прошла.',
        'PENDING' => 'Дата начала кампании еще не достигнута.',
        'SUSPENDED' => 'Кампания была приостановлена, вероятно, из-за нехватки выделенных средств.',
    ];
    public const STATUSES = [
      'UNKNOWN' => '',
      'ENABLED' => 'Кампания в настоящее время показывает объявления в зависимости от информации о бюджете.',
      'PAUSED' => 'Кампания была приостановлена пользователем.',
      'REMOVED' => 'Кампания была удалена.',
    ];
    private string $status;
    private string $state;
    private int $campaignId;

    public function __construct(string $status, string $state, int $campaignId, string $guid)
    {
        parent::__construct($guid);
        $this->status = $status;
        $this->state = $state;
        $this->campaignId = $campaignId;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getCampaignId(): int
    {
        return $this->campaignId;
    }
}
