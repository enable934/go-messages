<?php

declare(strict_types=1);

namespace Google\Message;

use Google\ResultFields\Forecast\StatsEstimate;

final class ForecastResultMessage extends BaseResultMessage
{
    /** @var array|StatsEstimate[] */
    private array $estimatedStats;

    public function __construct(array $estimatedStats, string $guid)
    {
        parent::__construct($guid);
        $this->estimatedStats = $estimatedStats;
    }

    /**
     * @return array|StatsEstimate[]
     */
    public function getEstimatedStats()
    {
        return $this->estimatedStats;
    }
}
