<?php

declare(strict_types=1);

namespace Google\Message;

final class ForecastCreateMessage extends BaseCreateMessage
{
    public const LANGUAGES = [
        'ru' => 1031,
        'uk' => 1036,
        'en' => 1000,
    ];
    /** @var array|string[] */
    private array $phrases;
    /** @var array|int[] */
    private array $geoIds;
    /** @var array|int[] */
    private array $languageIds;

    public function __construct(array $phrases, string $guid, array $geoIds = [], array $languageIds = [])
    {
        parent::__construct($guid);
        $this->phrases = $phrases;
        $this->geoIds = $geoIds;
        $this->languageIds = $languageIds;
    }

    /**
     * @return array|string[]
     */
    public function getPhrases()
    {
        return $this->phrases;
    }

    /**
     * @return array|int[]
     */
    public function getGeoIds()
    {
        return $this->geoIds;
    }

    /**
     * @return array|int[]
     */
    public function getLanguageIds()
    {
        return $this->languageIds;
    }
}
