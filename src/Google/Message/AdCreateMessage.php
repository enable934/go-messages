<?php

declare(strict_types=1);

namespace Google\Message;

final class AdCreateMessage extends BaseCreateMessage
{
    private string $clientCustomerId;
    private int $adGroupId;
    private string $headLine;
    private string $description;
    private string $href;
    private string $headLine2;

    public function __construct(
        string $clientCustomerId,
        int $adGroupId,
        string $headLine,
        string $headLine2,
        string $description,
        string $href,
        string $guid
    ) {
        parent::__construct($guid);
        $this->clientCustomerId = $clientCustomerId;
        $this->adGroupId = $adGroupId;
        $this->headLine = $headLine;
        $this->headLine2 = $headLine2;
        $this->description = $description;
        $this->href = $href;
    }

    public function getClientCustomerId(): string
    {
        return $this->clientCustomerId;
    }

    public function getAdGroupId(): int
    {
        return $this->adGroupId;
    }

    public function getHeadLine(): string
    {
        return $this->headLine;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getHref(): string
    {
        return $this->href;
    }

    public function getHeadLine2(): string
    {
        return $this->headLine2;
    }
}
