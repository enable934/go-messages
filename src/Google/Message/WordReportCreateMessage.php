<?php

declare(strict_types=1);

namespace Google\Message;

final class WordReportCreateMessage extends BaseCreateMessage
{
    private array $phrases;
    private array $geoId;

    public function __construct(array $phrases, string $guid, array $geoId = [])
    {
        $this->phrases = $phrases;
        parent::__construct($guid);
        $this->geoId = $geoId;
    }

    public function getPhrases(): array
    {
        return $this->phrases;
    }

    /**
     * @return array|int[]
     */
    public function getGeoId(): array
    {
        return $this->geoId;
    }
}
