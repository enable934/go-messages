<?php

declare(strict_types=1);

namespace Google\Message;

final class AdGetMessage extends BaseCreateMessage
{
    private string $clientCustomerId;
    private int $adGroupId;

    public function __construct(string $clientCustomerId, int $adGroupId, string $guid)
    {
        parent::__construct($guid);
        $this->clientCustomerId = $clientCustomerId;
        $this->adGroupId = $adGroupId;
    }

    public function getClientCustomerId(): string
    {
        return $this->clientCustomerId;
    }

    public function getAdGroupId(): int
    {
        return $this->adGroupId;
    }
}
