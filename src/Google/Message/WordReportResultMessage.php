<?php

declare(strict_types=1);

namespace Google\Message;

use Google\ResultFields\TargetingIdea;

final class WordReportResultMessage extends BaseResultMessage
{
    private array $targetingIdea;

    /**
     * KeywordIdeaResultMessage constructor.
     *
     * @param array|TargetingIdea[] $targetingIdea
     */
    public function __construct(array $targetingIdea, string $guid)
    {
        parent::__construct($guid);
        $this->targetingIdea = $targetingIdea;
    }

    /**
     * @return array|TargetingIdea[]
     */
    public function getTargetingIdea(): array
    {
        return $this->targetingIdea;
    }
}
