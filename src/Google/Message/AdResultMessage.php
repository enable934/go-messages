<?php

declare(strict_types=1);

namespace Google\Message;

final class AdResultMessage extends BaseResultMessage
{
    private int $id;

    public function __construct(int $id, string $guid)
    {
        parent::__construct($guid);
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
