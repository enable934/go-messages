<?php

declare(strict_types=1);

namespace Google\Message;

use Google\ResultFields\Keyword\KeywordResult;

final class KeywordResultMessage extends BaseResultMessage
{
    /** @var array|KeywordResult[] */
    private array $result;

    public function __construct(array $result, string $guid)
    {
        parent::__construct($guid);
        $this->result = $result;
    }

    /**
     * @return array|KeywordResult[]
     */
    public function getResult()
    {
        return $this->result;
    }
}
