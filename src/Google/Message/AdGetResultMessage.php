<?php

declare(strict_types=1);

namespace Google\Message;

final class AdGetResultMessage extends BaseResultMessage
{
    private string $status;
    private int $adId;

    public function __construct(string $status, int $adId, string $guid)
    {
        parent::__construct($guid);
        $this->status = $status;
        $this->adId = $adId;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getAdId(): int
    {
        return $this->adId;
    }
}
