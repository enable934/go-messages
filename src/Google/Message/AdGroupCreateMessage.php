<?php

declare(strict_types=1);

namespace Google\Message;

final class AdGroupCreateMessage extends BaseCreateMessage
{
    private string $clientCustomerId;
    private int $campaignId;
    private string $name;

    public function __construct(
        string $clientCustomerId,
        int $campaignId,
        string $name,
        string $guid
    ) {
        parent::__construct($guid);
        $this->clientCustomerId = $clientCustomerId;
        $this->campaignId = $campaignId;
        $this->name = $name;
    }

    public function getClientCustomerId(): string
    {
        return $this->clientCustomerId;
    }

    public function getCampaignId(): int
    {
        return $this->campaignId;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
