<?php

declare(strict_types=1);

namespace Google\Message;

final class KeywordAddMessage extends BaseCreateMessage
{
    /** @var array|string[] */
    private array $phrases;
    private int $adGroupId;
    private string $clientCustomerId;

    public function __construct(array $phrases, int $adGroupId, string $clientCustomerId, string $guid)
    {
        parent::__construct($guid);
        $this->phrases = $phrases;
        $this->adGroupId = $adGroupId;
        $this->clientCustomerId = $clientCustomerId;
    }

    public function getAdGroupId(): int
    {
        return $this->adGroupId;
    }

    public function getClientCustomerId(): string
    {
        return $this->clientCustomerId;
    }

    /**
     * @return array|string[]
     */
    public function getPhrases()
    {
        return $this->phrases;
    }
}
