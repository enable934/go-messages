<?php

declare(strict_types=1);

namespace Google\ResultFields\Keyword;

final class KeywordResult
{
    private string $keyword;
    private int $id;

    public function __construct(string $keyword, int $id)
    {
        $this->keyword = $keyword;
        $this->id = $id;
    }

    public function getKeyword(): string
    {
        return $this->keyword;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
