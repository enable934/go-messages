<?php

declare(strict_types=1);

namespace Google\ResultFields\Forecast;

final class StatsEstimate
{
    private float $averageCpc;
    private float $averagePosition;
    private float $clickThroughRate;
    private float $clicksPerDay;
    private float $impressionsPerDay;
    private float $totalCost;
    private string $phrase;

    public function __construct(string $phrase, float $averageCpc, float $averagePosition, float $clickThroughRate, float $clicksPerDay, float $impressionsPerDay, float $totalCost)
    {
        $this->averageCpc = $averageCpc;
        $this->averagePosition = $averagePosition;
        $this->clickThroughRate = $clickThroughRate;
        $this->clicksPerDay = $clicksPerDay;
        $this->impressionsPerDay = $impressionsPerDay;
        $this->totalCost = $totalCost;
        $this->phrase = $phrase;
    }

    public function getAverageCpc(): float
    {
        return $this->averageCpc;
    }

    public function getAveragePosition(): float
    {
        return $this->averagePosition;
    }

    public function getClickThroughRate(): float
    {
        return $this->clickThroughRate;
    }

    public function getClicksPerDay(): float
    {
        return $this->clicksPerDay;
    }

    public function getImpressionsPerDay(): float
    {
        return $this->impressionsPerDay;
    }

    public function getTotalCost(): float
    {
        return $this->totalCost;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }
}
