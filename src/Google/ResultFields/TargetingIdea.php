<?php

declare(strict_types=1);

namespace Google\ResultFields;

final class TargetingIdea
{
    private string $keyword;
    private int $searchVolume;
    private string $averageCPC;
    private float $competition;

    public function __construct(string $keyword, int $searchVolume, string $averageCPC, float $competition)
    {
        $this->keyword = $keyword;
        $this->searchVolume = $searchVolume;
        $this->averageCPC = $averageCPC;
        $this->competition = $competition;
    }

    public function getKeyword(): string
    {
        return $this->keyword;
    }

    public function getSearchVolume(): int
    {
        return $this->searchVolume;
    }

    public function getAverageCPC(): string
    {
        return $this->averageCPC;
    }

    public function getCompetition(): float
    {
        return $this->competition;
    }
}
